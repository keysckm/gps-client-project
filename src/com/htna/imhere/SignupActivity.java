package com.htna.imhere;

import java.util.regex.Pattern;

import com.htna.imhere.SessionOrder.MessageType;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SignupActivity extends Activity {

	private EditText 	mTxtId;
	private EditText 	mTxtPw;
	private EditText 	mTxtName;
	private Button		mBtnSignup;
	private Button		mBtnCancel;
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_signup);
	    // TODO Auto-generated method stub
	    
	    mTxtId = (EditText)findViewById(R.id.edit_id);
	    mTxtPw = (EditText)findViewById(R.id.edit_password);
	    mTxtName = (EditText)findViewById(R.id.edit_name);
	    mBtnSignup = (Button)findViewById(R.id.btn_signup);
	    mBtnCancel = (Button)findViewById(R.id.btn_signupcancel);
	    
	    final SignupActivity me = this;
	    
	    mBtnCancel.setOnClickListener(new OnClickListener(){
	    	
			@Override
			public void onClick(View v) {
				finish();
			}
		});
    	
	    mBtnSignup.setOnClickListener(new OnClickListener(){
	    	
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				final String email_pattern = "^[0-9A-Za-z]+(([\\.\\-_])[0-9A-Za-z]+)*@[0-9A-Za-z]+(([\\.\\-])[0-9A-Za-z-]+)*\\.[A-Za-z]{2,4}$";
				
				String email = mTxtId.getText().toString();
				
				if(mTxtId.length() == 0)
				{	// id 입력창이 비었음
					Toast.makeText(getApplicationContext(), getString(R.string.error_email_empty), Toast.LENGTH_SHORT).show();
				}
				if(Pattern.matches(email_pattern, email) == false)
				{
					Toast.makeText(getApplicationContext(), getString(R.string.error_email_pattern), Toast.LENGTH_SHORT).show();
				}
				else if(mTxtPw.length() == 0)
				{
					Toast.makeText(getApplicationContext(), getString(R.string.error_password_empty), Toast.LENGTH_SHORT).show();
				}
				else if(!(mTxtPw.length() >= 8 && mTxtPw.length() <= 16))
				{
					Toast.makeText(getApplicationContext(), getString(R.string.error_password_length), Toast.LENGTH_SHORT).show();
				}
				else if(mTxtName.length() == 0)
				{
					Toast.makeText(getBaseContext(), getString(R.string.error_name_empty), Toast.LENGTH_SHORT).show();
				}
				else if(mTxtName.length() > 10)
				{
					Toast.makeText(getBaseContext(), getString(R.string.error_name_length), Toast.LENGTH_SHORT).show();
				}
				else
				{
					byte[] buf = Messages.BUILD_REQ_SIGNUP(mTxtId.getText().toString(), mTxtPw.getText().toString(), mTxtName.getText().toString());
					new TCP().execute(new SessionOrder(me, MessageType.SEND, buf));
					// new TCP().execute(new SessionOrder(me, MessageType.RECV));
				}
			}
	    	
	    });
	}

}
