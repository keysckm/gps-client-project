package com.htna.imhere;

import java.util.regex.Pattern;

import com.htna.imhere.SessionOrder.MessageType;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class AddFriendDialog extends Dialog{

	Context mContext = null;
	
	TextView mTxtEmail = null;
	
	public AddFriendDialog(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		mContext = context;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.dialog_addfriend);
		
		// field init
		mTxtEmail = (TextView)findViewById(R.id.edit_dlg_addfriend_email);
		
		Button btn = (Button)findViewById(R.id.btn_dlg_addfriend_reg);
		btn.setOnTouchListener(new OnTouchListener(){

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				
				if(event.getAction() == MotionEvent.ACTION_UP)
				{
					final String email_pattern = "^[0-9A-Za-z]+(([\\.\\-_])[0-9A-Za-z]+)*@[0-9A-Za-z]+(([\\.\\-])[0-9A-Za-z-]+)*\\.[A-Za-z]{2,4}$";
					
					String email = mTxtEmail.getText().toString();
					if(email.isEmpty())
					{
						Toast.makeText(mContext, mContext.getString(R.string.error_email_empty), Toast.LENGTH_SHORT).show();
					}
					else if(email.length() > 32)
					{
						Toast.makeText(mContext, mContext.getString(R.string.error_email_length), Toast.LENGTH_SHORT).show();
					}
					else if(Pattern.matches(email_pattern, email) == false)
					{
						Toast.makeText(mContext, mContext.getString(R.string.error_email_pattern), Toast.LENGTH_SHORT).show();
					}
					else if(email.equals(UserInfo.Email))
					{
						Toast.makeText(mContext, mContext.getString(R.string.error_not_friend_me), Toast.LENGTH_SHORT).show();
					}
					else
					{					
						byte[] buf = Messages.BUILD_REQ_ADDFRIEND(email);
						SessionOrder order = new SessionOrder(mContext, MessageType.SEND, buf);
						new TCP().execute(order);
					}
				}
				
				return false;
			}
			
		});
	}
}
