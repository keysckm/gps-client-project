package com.htna.imhere;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

public class FriendListView extends LinearLayout {

	private Context mContext = null;
	private TextView mEmail = null;
	private TextView mName = null;
	private TextView mDatetime = null;
	private TextView mRelationship = null;
	
	public FriendListView(Context context) {
		super(context);

		mContext = context;
		
		// 레이아웃을 메모리상에 올린다.
		LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.item_friend, this, true);
		
		// 필드 초기화
		mEmail = (TextView)findViewById(R.id.lbl_item_friend_email);
		mName = (TextView)findViewById(R.id.lbl_item_friend_name);
		mDatetime = (TextView)findViewById(R.id.lbl_item_friend_datetime);
		mRelationship = (TextView)findViewById(R.id.lbl_item_friend_relationship);
	}
	
	public FriendListView(Context context, FriendInfo info) {
		super(context);

		mContext = context;
		
		// 레이아웃을 메모리상에 올린다.
		LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.item_friend, this, true);
		
		// 필드 초기화
		mEmail = (TextView)findViewById(R.id.lbl_item_friend_email);
		mName = (TextView)findViewById(R.id.lbl_item_friend_name);
		mDatetime = (TextView)findViewById(R.id.lbl_item_friend_datetime);
		mRelationship = (TextView)findViewById(R.id.lbl_item_friend_relationship);
		
		setData(info.email, info.name, info.datetime, info.relationship);
	}
	
	public void setData(String email, String name, String datetime, String relationship)
	{
		mEmail.setText(email);			// 이메일
		mName.setText(name);			// 이름
		mDatetime.setText(datetime);	// 등록일(혹은 친구요청일)
		if(relationship != null)
			mRelationship.setText(relationship);
		else
			mRelationship.setText("");;
	}
	
	public String getEmail()
	{
		return mEmail.getText().toString();
	}
	
	public String getName()
	{
		return mName.getText().toString();
	}
	
	public String getDatetime()
	{
		return mDatetime.getText().toString();
	}
}
