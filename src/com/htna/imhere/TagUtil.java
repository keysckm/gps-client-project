package com.htna.imhere;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.UUID;

import android.content.Context;

public class TagUtil{
	
	private static String read(File fd) throws IOException {
        RandomAccessFile f = new RandomAccessFile(fd, "r");
        int size = (int)f.length();
        
        byte[] bytes = new byte[size];
        f.readFully(bytes);
        f.close();
        
        return new String(bytes);
    }
	
	private static void write(File fd, String value) throws IOException
	{
        FileOutputStream out = new FileOutputStream(fd);
        out.write(value.getBytes());
        out.close();
	}
	
    public synchronized static String read_Tag(Context context, String tag)
    {
    	String strValue;
    	File fd = new File(context.getFilesDir(), tag);
    	try{
    		if(!fd.exists())	// 태그 없다!
    			return null;
    		
    		strValue = read(fd);
    	}
    	catch(Exception e){
    		throw new RuntimeException(e);
    	}
    	
    	return strValue;
    }
    
    public synchronized static void write_Tag(Context context, String tag, String value)
    {
    	File fd = new File(context.getFilesDir(), tag);
    	try{
    		write(fd, value);
    	}
    	catch(Exception e)
    	{
    		throw new RuntimeException(e);
    	}
    }

    private static void writeUuidFile(File fd) throws IOException {
        FileOutputStream out = new FileOutputStream(fd);
        String id = UUID.randomUUID().toString();
        out.write(id.getBytes());
        out.close();
    }
    
	public synchronized static String app_uuid(Context context){
		String uuid;
    	// 안드로이드 앱 디렉토리에서 uuid 파일을 오픈한다.
        File fd = new File(context.getFilesDir(), "uuid");
        try {
            if (!fd.exists())	// 만약 파일이 없다면 -> 최초 실행 -> uuid 생성
            	writeUuidFile(fd);	// uuid를 생성하고 쓴다.
            
            uuid = read(fd);	// uuid를 읽는다.
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return uuid;
	}
}