package com.htna.imhere;

import java.text.SimpleDateFormat;

import com.htna.imhere.SessionOrder.MessageType;

import android.app.Application;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.util.Log;

public class MyLocManager extends Application implements LocationListener {

	static SimpleDateFormat dayTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub

		Log.i("GPS", "Provider: " + location.getProvider() + 
				"Lat: " + location.getLatitude() + 
				"Lon: " + location.getLongitude() +
				"Speed: " + location.getSpeed() + 
				"Bearing: " + location.getBearing() + 
				"Accuracy: " + location.getAccuracy());
		
		byte[] buf = Messages.BUILD_CTS_GPSDATA(
				location.getLatitude(), 
				location.getLongitude(),
				(int)location.getSpeed(),
				(int)location.getBearing(),
				(int)location.getAccuracy(),
				dayTime.format(location.getTime()));
		
		new TCP().execute(new SessionOrder(ApplicationContextProvider.getContext(), MessageType.SEND, buf));
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}
	
}
