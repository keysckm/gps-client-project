package com.htna.imhere;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public class FriendListAdapter extends BaseAdapter {

	private Context mContext;	// ���ؽ�Ʈ
	private List<FriendInfo> mItems = new ArrayList<FriendInfo>();
	
	public FriendListAdapter(Context _context)
	{
		mContext = _context;
	}
	
	public void addItem(FriendInfo info){
		mItems.add(info);
	}
	
	public void removeItem(int position)
	{
		mItems.remove(position);
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mItems.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// �並 �����Ѵ�.
		FriendListView view = null;
		if(convertView == null)	// �� ����
			view = new FriendListView(mContext);
		else	// �並 ��Ȱ��
			view = (FriendListView)convertView;
		
		FriendInfo info = mItems.get(position);
		view.setData(info.email, info.name, info.datetime, info.relationship);
		
		return view;
	}
	
}
