package com.htna.imhere;

public class UserInfo {

	public static long UserToken = 0;
	public static String Email;
	public static String Name;
	
	// 마지막으로 서치된 위치
	public static double Latitude = 0.0;
	public static double Longitude = 0.0;
	public static int Speed = 0;
	public static int Bearing = 0;
	
	public static void clear()
	{
		UserToken = 0;
		Latitude = Longitude = 0;
		Speed = Bearing = 0;
		Email = Name = null;
	}
}
