package com.htna.imhere;

import android.app.Application;
import android.content.Context;
 
public class ApplicationContextProvider extends Application {
	// ApplicationContextProvider는 Activity나 Application을 상속받지 않은 클래스에서
	// Context를 얻고자 할 때 사용할 수 있음.
    /**
     * Keeps a reference of the application context
     */
    private static Context sContext;
 
    @Override
    public void onCreate() {
        super.onCreate();
 
        sContext = getApplicationContext();
 
    }
 
    /**
     * Returns the application context
     *
     * @return application context
     */
    public static Context getContext() {
        return sContext;
    }
 
}