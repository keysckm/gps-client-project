package com.htna.imhere;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

public class GetdateDialog extends Dialog implements OnDateSetListener, OnTimeSetListener, OnTouchListener{

	private Context mParentContext = null;

	final private String mstrTimeFormat = "yyyy년 MM월 dd일  HH시 mm분";
	private EditText mEditStart = null;
	private EditText mEditEnd = null;
	
	// 각 버튼
	private Button mbtnStartDate = null;
	private Button mbtnStartTime = null;
	private Button mbtnEndDate = null;
	private Button mbtnEndTime = null;
	private Button mBtnOk = null;
	private Button mBtnCancel = null;

	private Calendar mStartCal = null;	// 탐색 시작 시각
	private Calendar mEndCal = null;	// 탐색 종료 시각
	
	private boolean misStart = true; 	// start 버튼을 클릭했으면 true, end 버튼을 클릭했으면 false
	
	private boolean misCalled = false;		// 다이얼로그가 연속으로 호출되지 않도록 방지
	
	public boolean misOk = false;		// ok인지 cancel인지
	
	
	
	public GetdateDialog(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		mParentContext = context;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		setContentView(R.layout.dialog_getdate);
		
		mStartCal = Calendar.getInstance();
		mEndCal = Calendar.getInstance();
		
		mStartCal.set(
				mStartCal.get(Calendar.YEAR)
				, mStartCal.get(Calendar.MONTH)
				, mStartCal.get(Calendar.DAY_OF_MONTH)
				, 0, 0 ,0);
		
		mEndCal.set(
				mEndCal.get(Calendar.YEAR)
				, mEndCal.get(Calendar.MONTH)
				, mEndCal.get(Calendar.DAY_OF_MONTH)
				, mEndCal.get(Calendar.HOUR_OF_DAY)
				, mEndCal.get(Calendar.MINUTE)
				, 0);
		
		OnTouchListener okcancellistener = new OnTouchListener(){

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				if(v == mBtnOk)
				{
					if(mStartCal.getTimeInMillis() > mEndCal.getTimeInMillis())
					{
						Toast.makeText(mParentContext, "시작일시가 종료일시보다 늦습니다.", Toast.LENGTH_SHORT).show();
						return true;
					}
					misOk = true;
					dismiss();
				}
				else if(v == mBtnCancel)
				{
					misOk = false;
					dismiss();
				}
				
				return true;
			}
			
		};
		
		mBtnOk = (Button)findViewById(R.id.btnGetdateOk);
		mBtnCancel = (Button)findViewById(R.id.btnGetdateCancel);
		mBtnOk.setOnTouchListener(okcancellistener);
		mBtnCancel.setOnTouchListener(okcancellistener);
		
		mEditStart = (EditText)findViewById(R.id.editStartdate);
		mEditEnd = (EditText)findViewById(R.id.editEnddate);
		
		SimpleDateFormat df = new SimpleDateFormat("yyyy년 MM월 dd일  HH시 mm분");
		
		mEditStart.setText(df.format(mStartCal.getTime()));
		mEditEnd.setText(df.format(mEndCal.getTime()));
		
		mbtnStartDate = (Button)findViewById(R.id.btnStartDate);
		mbtnEndDate = (Button)findViewById(R.id.btnEndDate);
		mbtnStartTime = (Button)findViewById(R.id.btnStartTime);
		mbtnEndTime = (Button)findViewById(R.id.btnEndTime);
		
		mbtnStartDate.setOnTouchListener(this);
		mbtnEndDate.setOnTouchListener(this);
		mbtnStartTime.setOnTouchListener(this);
		mbtnEndTime.setOnTouchListener(this);
	}

	@Override
	public void onDateSet(DatePicker view, int year, int monthOfYear,
			int dayOfMonth) {
		// TODO Auto-generated method stub
		misCalled = false;
		
		EditText t = null;
		Calendar cal = null;
		if(misStart == true)
		{
			t = mEditStart;
			cal = mStartCal;
		}
		else if(misStart == false)
		{
			t = mEditEnd;
			cal = mEndCal;
		}
		try{
			cal.set(year, monthOfYear, dayOfMonth,
					cal.get(Calendar.HOUR_OF_DAY),
					cal.get(Calendar.MINUTE),
					cal.get(Calendar.SECOND));
			
			SimpleDateFormat df = new SimpleDateFormat(mstrTimeFormat);
			t.setText(df.format(cal.getTime()));
		}
		catch(NullPointerException e)
		{
			Log.e("GetdateDialog", e.getMessage());
			Toast.makeText(mParentContext, e.getMessage(), Toast.LENGTH_SHORT).show();
		}
	}
	
	@Override
	public void onTimeSet (TimePicker view, int hourOfDay, int minute)
	{
		misCalled = false;
		
		EditText t = null;
		Calendar cal = null;
		if(misStart == true)
		{
			t = mEditStart;
			cal = mStartCal;
		}
		else if(misStart == false)
		{
			t = mEditEnd;
			cal = mEndCal;
		}
		try{
			cal.set(cal.get(Calendar.YEAR),
					cal.get(Calendar.MONTH),
					cal.get(Calendar.DAY_OF_MONTH),
					hourOfDay, minute);
			
			SimpleDateFormat df = new SimpleDateFormat(mstrTimeFormat);
			t.setText(df.format(cal.getTime()));
		}
		catch(NullPointerException e)
		{
			Log.e("GetdateDialog", e.getMessage());
			Toast.makeText(mParentContext, e.getMessage(), Toast.LENGTH_SHORT).show();
		}
	}
	
	public Calendar getStartCal()
	{
		return mStartCal;
	}
	
	public Calendar getEndCal()
	{
		return mEndCal;
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		
		if(event.getAction() == MotionEvent.ACTION_UP && misCalled == false)
		{
			misCalled = true;
			Calendar cal = null;
			
			if(v == mbtnStartDate || v == mbtnStartTime)
			{
				misStart = true;
				cal = mStartCal;
			}
			else if(v == mbtnEndDate || v == mbtnEndTime)
			{
				misStart = false;
				cal = mEndCal;
			}
			
			if(v == mbtnStartDate || v == mbtnEndDate)
				new DatePickerDialog(mParentContext, this, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH)).show();
			else if(v == mbtnStartTime || v == mbtnEndTime)
				new TimePickerDialog(mParentContext, this, cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), true).show();
			
		}
		return false;
	}

}
