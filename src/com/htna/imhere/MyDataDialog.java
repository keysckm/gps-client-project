package com.htna.imhere;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.TextView;

public class MyDataDialog extends Dialog {
	
	private Context mParentContext = null;
	
	private TextView mLabelToken = null;
	private TextView mLabelEmail = null;
	private TextView mLabelName = null;
	private TextView mLabelLatitude = null;
	private TextView mLabelLongitude = null;
	private TextView mLabelSpeed = null;
	private TextView mLabelBearing = null;

	public MyDataDialog(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		mParentContext = context;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		setContentView(R.layout.dialog_mydata);
		
		// view init
		mLabelToken = (TextView)findViewById(R.id.lbl_dlg_mydata_usertoken);
		mLabelEmail = (TextView)findViewById(R.id.lbl_dlg_mydata_email);
		mLabelName = (TextView)findViewById(R.id.lbl_dlg_mydata_name);
		mLabelLatitude = (TextView)findViewById(R.id.lbl_dlg_mydata_latitude);
		mLabelLongitude = (TextView)findViewById(R.id.lbl_dlg_mydata_longitude);
		mLabelSpeed = (TextView)findViewById(R.id.lbl_dlg_mydata_speed);
		mLabelBearing = (TextView)findViewById(R.id.lbl_dlg_mydata_bearing);

		// �� ����
		if(UserInfo.UserToken == 0)
		{
			mLabelToken.setText(mParentContext.getString(R.string.label_unknown));
			mLabelEmail.setText(mParentContext.getString(R.string.label_unknown));
			mLabelName.setText(mParentContext.getString(R.string.label_unknown));
		}
		else
		{
			mLabelToken.setText(Long.toString(UserInfo.UserToken));
			mLabelEmail.setText(UserInfo.Email);
			mLabelName.setText(UserInfo.Name);
		}
		
		mLabelLatitude.setText(Double.toString(UserInfo.Latitude));
		mLabelLongitude.setText(Double.toString(UserInfo.Longitude));
		mLabelSpeed.setText(Integer.toString(UserInfo.Speed));
		mLabelBearing.setText(Integer.toString(UserInfo.Bearing));
		
		Button btnOk = (Button)findViewById(R.id.btn_dlg_mydata_ok);
		btnOk.setOnTouchListener(new OnTouchListener(){

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				dismiss();
				return false;
			}
			
		});
	}
}
