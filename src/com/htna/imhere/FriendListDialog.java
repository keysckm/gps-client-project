package com.htna.imhere;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.view.ViewGroup;
import android.widget.LinearLayout.LayoutParams;

public class FriendListDialog extends Dialog{

	private Context mContext = null;
	
	FriendListWidget mWidget = null;
	FriendListAdapter mAdapter = null;
	
	public FriendListDialog(Context context, FriendListWidget.OnDataSelectionListener listener) {
		super(context);
		mContext = context;
		
		init_widget(listener);
	}

	private void init_widget(FriendListWidget.OnDataSelectionListener listener)
	{
		// 화면을 구성한다		
		mWidget = new FriendListWidget(mContext);
		mAdapter = new FriendListAdapter(mContext);

		mWidget.setAdapter(mAdapter);
		mWidget.setOnDataSelectionListener(listener);
		
		ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(
				ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
		
		setContentView(mWidget, params);
	}
	
	public FriendListAdapter getAdapter()
	{
		return mAdapter;
	}
	
	public void addItem(FriendInfo info)
	{
		mAdapter.addItem(info);
		mAdapter.notifyDataSetChanged();
	}
	
	public void removdItem(int position)
	{
		mAdapter.removeItem(position);
		mAdapter.notifyDataSetChanged();
	}
}
