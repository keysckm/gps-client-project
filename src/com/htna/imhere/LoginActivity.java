package com.htna.imhere;

import java.util.regex.Pattern;

import com.htna.imhere.SessionOrder;
import com.htna.imhere.SessionOrder.MessageType;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends Activity {

	private EditText m_txt_id = null;
	private EditText m_txt_pw = null;	// 레이아웃으로부터 id, pw 텍스트박스를 가져옴
	private Button m_btn_login = null;	// 로그인버튼 가져옴 
	private Button m_btn_signup = null;
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_login);
	    // TODO Auto-generated method stub
	    
	    m_txt_id = (EditText) findViewById(R.id.txtid);
	    m_txt_pw = (EditText) findViewById(R.id.txtpw);
	    m_btn_login = (Button) findViewById(R.id.btnLogin);
	    m_btn_signup = (Button) findViewById(R.id.btnSignup);
	    final Context me = this;
	    
	    m_btn_signup.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(me, SignupActivity.class));
			}
	    
	    });
	    
	    m_btn_login.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// 여기에 로그인 처리를 한다.
				final String email_pattern = "^[0-9A-Za-z]+(([\\.\\-_])[0-9A-Za-z]+)*@[0-9A-Za-z]+(([\\.\\-])[0-9A-Za-z-]+)*\\.[A-Za-z]{2,4}$";
				
				String email = m_txt_id.getText().toString();
				String password = m_txt_pw.getText().toString();
				
				if(email.isEmpty())
				{	// id 입력창이 비었음
					Toast.makeText(getApplicationContext(), getString(R.string.error_email_empty), Toast.LENGTH_SHORT).show();
				}
				if(Pattern.matches(email_pattern, email) == false)
				{
					Toast.makeText(getApplicationContext(), getString(R.string.error_email_pattern), Toast.LENGTH_SHORT).show();
				}
				else if(m_txt_pw.length() == 0)
				{
					Toast.makeText(getApplicationContext(), getString(R.string.error_password_empty), Toast.LENGTH_SHORT).show();
				}
				else if(!(m_txt_pw.length() >= 8 && m_txt_pw.length() <= 16))
				{
					Toast.makeText(getApplicationContext(), getString(R.string.error_password_length), Toast.LENGTH_SHORT).show();
				}
				else
				{
					// 로그인 시도
					byte[] buf = Messages.BUILD_REQ_LOGIN(email, password);
					new TCP().execute(new SessionOrder(me, MessageType.SEND, buf));
				}
			}
	    	
	    });
	}
	
	public void callback_RespLogin_Accept()
	{
		byte[] buf = Messages.BUILD_REQ_GETMYDATA();
		new TCP().execute(new SessionOrder(this, MessageType.SEND, buf));
	}

	public void Callback_RespMyData()
	{
		startActivity(new Intent(this, MapActivity.class));
		finish();
	}
}
