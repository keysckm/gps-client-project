package com.htna.imhere;

import java.io.UnsupportedEncodingException;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class AddAlarmzoneDialog extends Dialog implements OnTouchListener {
	
	private Context		mParentContext = null;
	
	private double		mdLatitude;
	private double		mdLongitude;
	private String		mstrAddress = null;
	
	private TextView	mTxtLatitude = null;
	private TextView	mTxtLongitude = null;
	private TextView	mTxtAddress = null;
	private Button 		mBtnOk = null;
	private Button 		mBtnCancel = null;
	private EditText 	mEditNick = null;
	private EditText 	mEditDistance = null;
	
	private String		mstrNick = null;
	private int			mDistance;
	
	public boolean		misOk = false;
	
	public AddAlarmzoneDialog(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		mParentContext = context;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dialog_alarmzone);
		
		mTxtLatitude = (TextView)findViewById(R.id.txtlatitude);
		mTxtLatitude.setText(Double.toString(mdLatitude));
		mTxtLongitude = (TextView)findViewById(R.id.txtlongitude);
		mTxtLongitude.setText(Double.toString(mdLongitude));
		mTxtAddress = (TextView)findViewById(R.id.txtaddress);
		if(mstrAddress != null)
			mTxtAddress.setText(mstrAddress);
		else
			mTxtAddress.setText("알 수 없음");
		
		mBtnOk = (Button)findViewById(R.id.btnOk);
		mBtnCancel = (Button)findViewById(R.id.btnCancel);
		mEditNick = (EditText)findViewById(R.id.editNickname);
		mEditDistance = (EditText)findViewById(R.id.editDistance);
		
		mBtnOk.setOnTouchListener(this);
		mBtnCancel.setOnTouchListener(this);
	}
	
	public void setLatitude(double _dlat)
	{
		mdLatitude = _dlat;
	}
	
	public void setLongitude(double _dlon)
	{
		mdLongitude = _dlon;
	}
	
	public void setAddress(String _strAddr)
	{
		mstrAddress = _strAddr;
	}
	
	public int getDistance()
	{
		return mDistance;
	}
	
	public String getNickname()
	{
		return mstrNick;
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		if(v == mBtnOk)
		{
			mstrNick = mEditNick.getText().toString();
			if(mstrNick.length() <= 0)
			{
				Toast.makeText(mParentContext, "별칭을 입력해주세요.", Toast.LENGTH_SHORT).show();
				return true;
			}
			
			if(mEditDistance.length() <= 0)
			{
				Toast.makeText(mParentContext, "안전구역 범위를 설정해주세요.", Toast.LENGTH_SHORT).show();
				return true;
			}
			
			try {
				if(mEditDistance.getText().toString().getBytes("UTF-8").length > 64)
				{
					Toast.makeText(mParentContext, "별칭이 너무 길어요!", Toast.LENGTH_SHORT).show();
					return true;
				}
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			}
			
			mDistance = Integer.parseInt(mEditDistance.getText().toString());
			misOk = true;
		}
		else if(v == mBtnCancel)
		{
			misOk = false;
		}
		
		dismiss();
		
		return true;
	}

}
