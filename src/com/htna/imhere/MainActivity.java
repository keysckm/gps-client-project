package com.htna.imhere;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.TextView;

import com.google.android.gcm.GCMRegistrar;
import com.htna.imhere.SessionOrder.MessageType;

public class MainActivity extends Activity {

	public class MainActivityHandler extends Handler{
				
		static final int MSG_ONREGIST = 10;
		
		@Override
		public void handleMessage(Message msg){
			callback_onRegist();
		}
	}
	
	public static MainActivity Activity = null;
	private TextView mtxtLoadState = null;
	
	public MainActivityHandler mHandler = null;
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_main);
	    // TODO Auto-generated method stub
	    Activity = this;
	    // 핸들러 생성
	    mHandler = new MainActivityHandler();
	    
	    mtxtLoadState = (TextView)findViewById(R.id.txtLoadState);
	    
	    mtxtLoadState.setText(getString(R.string.init_please_wait));
	    // 토큰 정보 획득
	    Token.Init(getApplicationContext());
	    Log.i("INIT", "Token Init");
	    registerDevice();	// GCM 서비스 등록
	    Log.i("INIT", "RegisterDevice");
	    
	    //mtxtLoadState.setText(getString(R.string.connect_load_state_connect));
	    //new TCP().execute(new SessionOrder(this, MessageType.CONNECT));
	}
	
	public void callback_onRegist()
	{
		setLoadState(R.string.connect_load_state_connect);
	    new TCP().execute(new SessionOrder(this, MessageType.CONNECT));
	}
	
	public void callback_connected()	// 접속됨
	{
		// 서버에 엑세스 요청
		Log.i("TCP", "Request Access");
		Log.v("TOKEN", "Device: " + Token.getDeviceToken() + ", User: " + Token.getUserToken());
		byte[] buf = Messages.BUILD_REQ_ACCESS();	// 접속요청
		new TCP().execute(new SessionOrder(this, MessageType.SEND, buf));
	}
	
	public void Callback_RespAccessApproved()	// 접속이 허가됨
	{
		// Device토큰과 User토큰이 모두 유효하여 인증이 성공하였음
		//startActivity(new Intent(this, MapActivity.class));
		//finish();
		setLoadState(R.string.connect_load_userinfo);
		byte[] buf = Messages.BUILD_REQ_GETMYDATA();
		new TCP().execute(new SessionOrder(this, MessageType.SEND, buf));
	}
	
	public void Callback_RespMyData()
	{
		setLoadState(R.string.connect_load_state_complete);
		startActivity(new Intent(this, MapActivity.class));
		finish();
	}
	
	public void Callback_RespRegidApproved()	// RegID가 수리됨
	{
		startActivity(new Intent(this, LoginActivity.class));
		finish();
	}
	
	public void setLoadState(int res)
	{
		setLoadState(getString(res));
	}
	
	public void setLoadState(String state)
	{
		mtxtLoadState.setText(state);
	} 
	
	public void registerDevice()
	{
		GCMRegistrar.checkDevice(this);
		GCMRegistrar.checkManifest(this);
		
		// 단말의 regID를 받아옴
		final String regId = GCMRegistrar.getRegistrationId(this);
		if(regId.equals(""))
		{	// 단말이 등록되지 않았으므로 단말을 등록함
			GCMRegistrar.register(getBaseContext(), GCMServiceInfo.PROJECT_ID);
		}
		else
		{	// 단말이 등록되어있는지 확인함
			if(GCMRegistrar.isRegisteredOnServer(this))
				Log.d(GCMServiceInfo.TAG, "단말이 이미 등록되어있음.");
			else	// 단말이 등록되어있지 않음. 등록함
				GCMRegistrar.register(getBaseContext(), GCMServiceInfo.PROJECT_ID);
		}
	}

}
