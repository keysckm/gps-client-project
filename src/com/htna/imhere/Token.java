package com.htna.imhere;

import android.content.Context;

public class Token {
	
	enum TokenType{
		DEVICETOKEN,
		USERTOKEN
	}
	
	private static long DeviceToken;
	private static long UserToken;
	
	public static void Init(Context context)
	{
		String tag = TagUtil.read_Tag(context, TokenType.DEVICETOKEN.toString());
		if(tag == null)
			DeviceToken = 0;
		else
			DeviceToken = Long.parseLong(tag);
		
		tag = TagUtil.read_Tag(context, TokenType.USERTOKEN.toString());
		if(tag == null)
			UserToken = 0;
		else
			UserToken = Long.parseLong(tag);		
	}
	
	public static long getDeviceToken()
	{
		return DeviceToken;
	}
	
	public static long getUserToken()
	{
		return UserToken;
	}
	
	public static void setDeviceToken(Context context, long device)
	{
		TagUtil.write_Tag(context, TokenType.DEVICETOKEN.toString(), Long.toString(device));
	}
	
	public static void setUserToken(Context context, long device)
	{
		TagUtil.write_Tag(context, TokenType.USERTOKEN.toString(), Long.toString(device));
	}
}
