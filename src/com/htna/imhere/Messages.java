package com.htna.imhere;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class Messages {
	
	// message index
	public static final int MSG_UNKNOWN = 0;
	public static final int REQ_ACCESS = 100;						// 앱 실행시 기본정보를 서버로 송신
	public static final int REQ_SIGNUP = REQ_ACCESS+1;				// 회원가입
	public static final int REQ_LOGIN = REQ_SIGNUP+1;				// 로그인
	public static final int REQ_TOKENLOGIN = REQ_LOGIN+1;			// 토큰 로그인
	public static final int REQ_LOGOUT = REQ_TOKENLOGIN + 1;		// 로그아웃
	public static final int REQ_LOCATIONDATA = REQ_LOGOUT+1;		// GPS데이터 요청
	public static final int REQ_GETALARMZONE = REQ_LOCATIONDATA+1;	// 알람구역 요청
	public static final int REQ_ADDALARMZONE = REQ_GETALARMZONE+1;	// 알람구역 추가
	public static final int REQ_DELALARMZONE = REQ_ADDALARMZONE+1;	// 알람구역 삭제
	public static final int REQ_MODALARMZONE = REQ_DELALARMZONE+1;	// 알람구역 수정
	public static final int REQ_ADDALARMTARG = REQ_MODALARMZONE+1;	// 알람구역에 따라 푸시를 받을 등록자 추가
	public static final int REQ_DELALARMTARG = REQ_ADDALARMTARG+1;	//				"					  삭제
	public static final int REQ_MODALARMTARG = REQ_DELALARMTARG+1;	//				"					  수정
	public static final int REQ_GETMYDATA = REQ_MODALARMTARG + 1;	// 나 자신에 대한 데이터 요청
	public static final int REQ_FINDFRIEND = REQ_GETMYDATA + 1;		// 친구 검색
	public static final int REQ_ADDFRIEND = REQ_FINDFRIEND + 1;		// 친구 등록 요청
	public static final int REQ_GETFRIENDLIST = REQ_ADDFRIEND + 1;	// 친구 목록 요청
	public static final int REQ_GETFRIENDREQLIST = REQ_GETFRIENDLIST + 1;	// 친구 추가 요청 리스트를 요청(...)
	public static final int REQ_RESPFRIENDREQ = REQ_GETFRIENDREQLIST + 1;	// 친구 요청에 대한 응답
			
	public static final int RESP_RESULT = 200;						// 받은 요청에 대한 결과
	public static final int RESP_DEVICETOKEN = RESP_RESULT + 1;		// 디바이스 토큰
	public static final int RESP_USERTOKEN = RESP_DEVICETOKEN + 1;	// 유저 토큰
	public static final int RESP_TOKEN = RESP_USERTOKEN + 1;		// 로그인토큰 송신
	public static final int RESP_LOCATIONDATA = RESP_TOKEN + 1;		// GPS 데이터
	public static final int RESP_ALARMZONE = RESP_LOCATIONDATA + 1;	// ALARMZONE 데이터
	public static final int RESP_ALARMTARG = RESP_ALARMZONE + 1;	// ALARMZONE 대상자 데이터 송신
	public static final int RESP_ADDALARMZONE = RESP_ALARMTARG + 1;	// 새로 추가된 알람존의 인덱스
	public static final int RESP_GETMYDATA = RESP_ADDALARMZONE + 1;	// REQ_GETMYDATA에 대한 답신
	public static final int RESP_GETFRIENDLIST = RESP_GETMYDATA + 1;// 친구 리스트 요청에 대한 답신
	public static final int RESP_GETFRIENDREQLIST = RESP_GETFRIENDLIST + 1;	// 친구 추가 요청 리스트에 대한 요청의 답신... 뭐 말이 이상하노
	
	public static final int CTS_GPSDATA = 300;				// GPS데이터
	public static final int CTS_ECALL = CTS_GPSDATA + 1;	// 비상호출
	public static final int CTS_REGID = CTS_ECALL + 1;		// REGID
	
	// request result
	public static final int UNKNOWN = 0;						// 알 수 없는 요청
	public static final int SERVERERR = UNKNOWN + 1;			// 서버 에러
	public static final int APPROVED = SERVERERR + 1;			// 승인
	public static final int REFUSAL = APPROVED + 1;				// 거절
	public static final int INVALIDTOKEN = REFUSAL + 1;			// 무효한 토큰
	public static final int DEVICETOKEN = INVALIDTOKEN + 1;		// 새로운 디바이스 토큰
	public static final int USERTOKEN = DEVICETOKEN + 1;		// 새로운 유저 토큰
	public static final int NOTFOUND = USERTOKEN + 1;			// 찾을 수 없음
	public static final int ALREADY = NOTFOUND + 1;				// 이미 되어있음
	
	private static ByteBuffer MakeHeader(int _Idx, int _Size)
	{
		ByteBuffer buf = ByteBuffer.allocate(_Size);
		buf.clear();
		buf.order(ByteOrder.LITTLE_ENDIAN);
		
		buf.putInt(_Idx);
		buf.putInt(_Size);
		
		return buf;
	}
	
	// REQ_ACCESS
	public static byte[] BUILD_REQ_ACCESS()
	{
		/* MSG_REQ_ACCESS Struct Members
		 *  [TYPE]			[NAME]		[SIZE]		[COMMAND]
		 * 	MSG_IDX			nIdx		4			메시지 종류
		 *	unsigned int	nSize		4			메시지 길이
		 *	TOKEN			nDevice		8			디바이스 토큰
		 *	TOKEN			nUser		8			유저 토큰
		 */
		
		final int size = 24;	// 4+4+8+8 
		
		ByteBuffer buf = MakeHeader(REQ_ACCESS, size);
		buf.putLong(Token.getDeviceToken());
		buf.putLong(Token.getUserToken());

		return buf.array();
	}
	
	// CTS_GPSDATA
	public static byte[] BUILD_CTS_GPSDATA(double latitude, double longitude, int speed, int bearing, int accuracy, String time)
	{
		/* MSG_CTS_GPSDATA Struct Members
		 *  [TYPE]			[NAME]		[SIZE]		[COMMAND]
		 * 	MSG_IDX			nIdx		4			메시지 종류
		 *	unsigned int	nSize		4			메시지 길이
		 *	double			latitude	8			위도
		 *	double			longitude	8			경도
		 *	int				speed		4			가속도
		 *	int				bearing		4			방향
		 *	int				accuracy	4			오차
		 *	char			time		19			UTC Date Time, yyyy-mm-dd hh:mm:dd
		 */
		
		final int size = 55;	//4+4+8+8+8+8+4+4+4+19
		
		ByteBuffer buf = MakeHeader(CTS_GPSDATA, size);
		
		buf.putDouble(latitude);	// 위도
		buf.putDouble(longitude);	// 경도
		buf.putInt(speed);			// 가속도
		buf.putInt(bearing);		// 방향
		buf.putInt(accuracy);		// 오차
		buf.put(time.getBytes());	// time
		
		return buf.array();
	}
	
	// CTS_ECALL
	public static byte[] BUILD_CTS_ECALL()
	{
		/* MSG_CTS_ECALL Struct Members
		 *  [TYPE]			[NAME]		[SIZE]		[COMMAND]
		 * 	MSG_IDX			nIdx		4			메시지 종류
		 *	unsigned int	nSize		4			메시지 길이
		 */
		
		final int size = 8;
		ByteBuffer buf = MakeHeader(CTS_ECALL, size);
		
		return buf.array();
	}
	
	public static byte[] BUILD_CTS_REGID(String regid)
	{
		/* MSG_REQ_REGID Struct Members
		 *  [TYPE]			[NAME]		[SIZE]		[COMMAND]
		 * 	MSG_IDX			nIdx		4			메시지 종류
		 *	unsigned int	nSize		4			메시지 길이
		 *	int				regid_len	4			regid 길이
		 *	char[]			buf			<=512		regid
		 */
		
		final int regid_len = regid.length();	// regid의 길이는 어느정도 랜덤성을 띄는 것 같다.
		final int size = 12 + regid_len;			// regid의 길이를 모르면 사이즈를 확정할 수 없다.
		
		ByteBuffer buf = MakeHeader(CTS_REGID, size);
		
		buf.putInt(regid_len);		// regid's length
		buf.put(regid.getBytes());	// regid
		
		return buf.array();
	}
	
	// REQ_LOCATIONDATA
	public static byte[] BUILD_REQ_LOCATIONDATA(String strStart, String strEnd)
	{
		/* MSG_REQ_LOCATIONDATA Struct Members
		 *  [TYPE]			[NAME]		[SIZE]		[COMMAND]
		 * 	MSG_IDX			nIdx		4			메시지 종류
		 *	unsigned int	nSize		4			메시지 길이
		 *	char			start		19			UTC Date Time, yyyy-mm-dd hh:mm:dd
		 *	char			end			19			UTC Date Time, yyyy-mm-dd hh:mm:dd
		 *	int				nCnt		4			UserToken 갯수
		 */
		final int size = 46;
		
		ByteBuffer buf = MakeHeader(REQ_LOCATIONDATA, size);
		
		buf.put(strStart.getBytes());	// start
		buf.put(strEnd.getBytes());		// end
		
		return buf.array();
	}
	
	public static byte[] BUILD_REQ_SIGNUP(String email, String pw, String name)
	{
		/* MSG_REQ_SIGNUP Struct Members
		 *  [TYPE]			[NAME]		[SIZE]		[COMMAND]
		 * 	MSG_IDX			nIdx		4			메시지 종류
		 *	unsigned int	nSize		4			메시지 길이
		 *	int				id_len		4			아이디(이메일) 길이
		 *	int				name_len	4			닉네임(이름) 길이
		 *	char[]			Password	40			패스워드 해시
		 *	char[]			buf			32+50		ID, 이름 합성버퍼
		 */
		
		// 이름은 대부분 영문자가 아닌 문자가 들어감. utf8로 인코딩
		byte[] name_buf = name.getBytes();
		byte[] email_buf = email.getBytes();
		final int size = 56 + name_buf.length + email_buf.length;
		ByteBuffer buf = MakeHeader(REQ_SIGNUP, size);

		String hash = SHA1.getHash(pw);
		buf.putInt(email_buf.length);	// email length
		buf.putInt(name_buf.length);	// name length
		buf.put(hash.getBytes());	// hash
		buf.put(email_buf);
		buf.put(name_buf);
		return buf.array();
	}
	
	public static byte[] BUILD_REQ_LOGIN(String email, String pw)
	{
		/* MSG_REQ_LOGIN Struct Members
		 *  [TYPE]			[NAME]				[SIZE]		[COMMAND]
		 * 	MSG_IDX			nIdx				4			메시지 종류
		 *	unsigned int	nSize				4			메시지 길이
		 * 	int 			id_len				4			ID 길이
		 *	char[]			Password			40			패스워드 해시
		 *	char[]			Email				32			이메일
		 */
		
		final int size = 84;
		ByteBuffer buf = MakeHeader(REQ_LOGIN, size);
		
		String hash = SHA1.getHash(pw);
		buf.putInt(email.length());
		buf.put(hash.getBytes());
		buf.put(email.getBytes());
		
		return buf.array();
	}
	
	public static byte[] BUILD_REQ_GETALARMZONE()
	{
		/* MSG_REQ_GETALARMZONE Struct Members
		 *  [TYPE]			[NAME]				[SIZE]		[COMMAND]
		 * 	MSG_IDX			nIdx				4			메시지 종류
		 *	unsigned int	nSize				4			메시지 길이
		 */
		
		final int size = 8;
		
		ByteBuffer buf = MakeHeader(REQ_GETALARMZONE, size);
		return buf.array();
	}
	
	public static byte[] BUILD_REQ_ADDALARMZONE(double _latitude, double _longitude, int _distance, String _nick)
	{
		/* MSG_REQ_ADDALARMZONE Struct Members
		 *  [TYPE]			[NAME]				[SIZE]		[COMMAND]
		 * 	MSG_IDX			nIdx				4			메시지 종류
		 *	unsigned int	nSize				4			메시지 길이
		 * 	double			latitude;			8			위도
		 *	double			longitude;			8			경도
		 *	int				distance;			4			반경
		 *	char[]			nick[MAX_NICK_LEN];	50			알람존의 별칭
		 */
		
		final int size = 78;
		ByteBuffer buf = MakeHeader(REQ_ADDALARMZONE, size);
		
		buf.putDouble(_latitude);
		buf.putDouble(_longitude);
		buf.putInt(_distance);
		buf.put(_nick.getBytes());
		
		return buf.array();
	}
	
	public static byte[] BUILD_REQ_GETMYDATA()
	{
		/* MSG_REQ_ADDALARMZONE Struct Members
		 *  [TYPE]			[NAME]				[SIZE]		[COMMAND]
		 * 	MSG_IDX			nIdx				4			메시지 종류
		 *	unsigned int	nSize				4			메시지 길이
		 */
		
		final int size = 8;
		ByteBuffer buf = MakeHeader(REQ_GETMYDATA, size);
		
		return buf.array();
	}
	
	public static byte[] BUILD_REQ_LOGOUT()
	{
		/* MSG_REQ_ADDALARMZONE Struct Members
		 *  [TYPE]			[NAME]				[SIZE]		[COMMAND]
		 * 	MSG_IDX			nIdx				4			메시지 종류
		 *	unsigned int	nSize				4			메시지 길이
		 */
		
		final int size = 8;
		ByteBuffer buf = MakeHeader(REQ_LOGOUT, size);
		
		return buf.array();
	}
	
	public static byte[] BUILD_REQ_ADDFRIEND(String _email)
	{
		/* MSG_REQ_ADDFRIEND Struct Members
		 *  [TYPE]			[NAME]				[SIZE]		[COMMAND]
		 * 	MSG_IDX			nIdx				4			메시지 종류
		 *	unsigned int	nSize				4			메시지 길이
		 *	int 			email_len			4			길이
		 *	char[]			email[]				<= 32		아이디
		 */
		
		byte[] email_buf = _email.getBytes();
		final int size = 12 + email_buf.length;
		
		ByteBuffer buf = MakeHeader(REQ_ADDFRIEND, size);
		buf.putInt(email_buf.length);
		buf.put(email_buf);
		
		return buf.array();
	}
	
	public static byte[] BUILD_REQ_GETFRIENDLIST()
	{
		/* MSG_REQ_GETFRIENDLIST Struct Members
		 *  [TYPE]			[NAME]				[SIZE]		[COMMAND]
		 * 	MSG_IDX			nIdx				4			메시지 종류
		 *	unsigned int	nSize				4			메시지 길이
		 */
		final int size = 8;
		ByteBuffer buf = MakeHeader(REQ_GETFRIENDLIST, size);
		
		return buf.array();
	}
	
	public static byte[] BUILD_REQ_GETFRIENDREQLIST()
	{
		/* MSG_REQ_GETFRIENDREQLIST Struct Members
		 *  [TYPE]			[NAME]				[SIZE]		[COMMAND]
		 * 	MSG_IDX			nIdx				4			메시지 종류
		 *	unsigned int	nSize				4			메시지 길이
		 */
		final int size = 8;
		ByteBuffer buf = MakeHeader(REQ_GETFRIENDREQLIST, size);
		
		return buf.array();
	}
	
	public static byte[] BUILD_REQ_RESPFRIENDREQ(String email, int result)
	{
		/* MSG_REQ_RESPFRIENDREQ Struct Members
		 *  [TYPE]			[NAME]				[SIZE]		[COMMAND]
		 * 	MSG_IDX			nIdx				4			메시지 종류
		 *	unsigned int	nSize				4			메시지 길이
		 *	REQUEST_RESULT	resp				4			응답종류(승낙/거절)
		 *	int				nEmailLen;			4			이메일 길이
		 *	char[]			szMail				<=32		이메일
		 */
		
		byte[] email_buf = email.getBytes();
		final int size = 16 + email_buf.length;
		
		ByteBuffer buf = MakeHeader(REQ_RESPFRIENDREQ, size);
		buf.putInt(result);
		buf.putInt(email_buf.length);
		buf.put(email_buf);
		
		return buf.array();
	}
}
