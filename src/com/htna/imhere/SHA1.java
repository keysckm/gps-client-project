package com.htna.imhere;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SHA1 {
	public static String getHash(String s)
	{
		try {
			MessageDigest hash = MessageDigest.getInstance("SHA-1");
			hash.update(s.getBytes());	// password의 sha-1 해시값을 얻는다.
			byte messageDigest[] = hash.digest();
			StringBuffer strHex = new StringBuffer();
			for(int i = 0; i<messageDigest.length; i++)
			{	// byte버퍼에 있는 해시값을 스트링으로 변환
				if((0xFF & messageDigest[i]) == 0)
				{
					strHex.append(0);
					strHex.append(0);
					continue;
				}
				else if((0xF0 & messageDigest[i]) == 0)
				{	// 만약 상위바이트가 0이라면 append할 때 한글자만 들어간다.
					// 해당 문제를 방지하기 위해서...
					strHex.append(0);
				}
				
				strHex.append(Integer.toHexString(0xFF & messageDigest[i]));
			}
			return strHex.toString();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}
}
