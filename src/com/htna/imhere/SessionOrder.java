package com.htna.imhere;
import android.content.Context;

public class SessionOrder {
	enum MessageType{
		NONE,
		CONNECT,
		DISCONNECT,
		SEND,
		RECV,
	}
	
	private MessageType			type	= MessageType.NONE;
	private String				args	= null;
	private java.lang.Object	obj		= null;
	private byte[]				buf		= null;
	private Context				con		= null;
	
	public SessionOrder(Context activity, MessageType m)
	{
		this.con = activity;
		this.setType(m);
	}
	
	public SessionOrder(Context activity, MessageType m, String args)
	{
		this.con = activity;
		this.setType(m);
		this.setArgs(args);
	}
	
	public SessionOrder(Context activity, MessageType m, java.lang.Object obj)
	{
		this.con = activity;
		this.setType(m);
		this.setObj(obj);
	}

	public SessionOrder(Context activity, MessageType m, byte[] buf)
	{
		this.con = activity;
		this.setType(m);
		this.setBuf(buf);
	}
	
	public MessageType getType() {
		return type;
	}

	public void setType(MessageType type) {
		this.type = type;
	}

	public java.lang.Object getObj() {
		return obj;
	}

	public void setObj(java.lang.Object obj) {
		this.obj = obj;
	}

	public String getArgs() {
		return args;
	}

	public void setArgs(String args) {
		this.args = args;
	}

	public byte[] getBuf() {
		return buf;
	}

	public void setBuf(byte[] buf) {
		this.buf = buf;
	}

	public Context getContext() {
		return con;
	}
	
	
}