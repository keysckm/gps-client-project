package com.htna.imhere;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;

import com.google.android.gms.ads.identifier.AdvertisingIdClient.Info;
import com.htna.imhere.SessionOrder.MessageType;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

public class TCP extends AsyncTask<SessionOrder, Void, TCP.Result> {

	enum Result{
		Done,
		Fail,
		UnknownHost,
		Timeout,
		IOexception;
	}
	
	// 서버 주소
	public static final String serveraddr = "server.htna.kr";
	public static final int port = 32400;	// 포트번호

	private static String error_msg = "Done";
	
	// socket
	private static Socket sock = null;
	
	// 스트림
	private static BufferedOutputStream 	out = null;
	private	static BufferedInputStream		in = null;

	// 버퍼
	private static final short buf_size = 1024;
	private byte[] RecvBuf = null;
	private int	RecvSize = 0;
	
	// 명령
	SessionOrder order = null;
	
	// 연결상태 확인
	public static boolean isConnect()
	{
		if(sock == null)
			return false;

		return sock.isConnected();
	}
	
	public static String getLastError()
	{
		return error_msg;
	}
	
	protected Result Connect()
	{
		try{
			sock = new Socket(serveraddr, port);
			
			in = new BufferedInputStream(sock.getInputStream());
			out = new BufferedOutputStream(sock.getOutputStream());
		}
		catch(UnknownHostException e)
		{
			e.printStackTrace();
			error_msg = e.getMessage();
			return Result.UnknownHost;
		}
		catch(IOException e)
		{
			e.printStackTrace();
			error_msg = e.getMessage();
			return Result.Timeout;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			error_msg = e.getMessage();
			return Result.Fail;
		}
		return Result.Done;
	}
	
	protected void Disconnect()
	{
		try {
			if(out != null)
				out.close();
			
			if(in != null)
				in.close();
			
			if(sock != null)
				sock.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public boolean ProcessMsg(byte[] buf)
	{
		/* MSG_REQ_LOGIN Struct Members
		 *  [TYPE]			[NAME]				[SIZE]		[COMMAND]
		 * 	MSG_IDX			nIdx				4			// 메시지 종류
		 *	unsigned int	nSize				4			// 메시지 길이
		 */
		
		ByteBuffer bb = ByteBuffer.allocate(buf.length);
		bb.put(buf);
		if(bb.order() == ByteOrder.BIG_ENDIAN)	// 바이트오더 설정
			bb.order(ByteOrder.LITTLE_ENDIAN);
		
		int pos = 0;
		int command = bb.getInt(pos);
		pos += 4;
		int size = bb.getInt(pos);
		pos += 4;
		
		if(buf.length < size)
			return false;	// 크기가 안맞음
		
		switch(command)
		{
		case Messages.RESP_RESULT:	// Result
		{
			/* MSG_RESP_RESULT Struct Members
			 *  [TYPE]			[NAME]				[SIZE]		[COMMAND]
			 *  MSG_IDX			idx					4			이 메시지의 RESULT
			 * 	REQUEST_RESULT 	result				4			Result 종류
			 */
			int idx = bb.getInt(pos);
			pos += 4;
			int result = bb.getInt(pos);
			
			if(order.getContext() != null)
			{
				switch(result)
				{
				case Messages.UNKNOWN:
					Toast.makeText(order.getContext(), order.getContext().getString(R.string.error_unknown), Toast.LENGTH_SHORT).show();
					return true;
					
				case Messages.SERVERERR:
					Toast.makeText(order.getContext(), order.getContext().getString(R.string.error_servererr), Toast.LENGTH_SHORT).show();
					return true;
					
				case Messages.INVALIDTOKEN:
					Toast.makeText(order.getContext(), order.getContext().getString(R.string.error_invalid_token), Toast.LENGTH_SHORT).show();
					order.getContext().startActivity(new Intent(order.getContext(), LoginActivity.class));
					Token.setUserToken(order.getContext(), 0);
					break;
				}
			}
			
			switch(idx)
			{
			case Messages.REQ_ACCESS:	// 접속 결과
			{
				switch(result)
				{
				case Messages.APPROVED:	// 접속 허가
					MainActivity activity = (MainActivity)order.getContext();
					activity.Callback_RespAccessApproved();
					break;
				}
				
				break;
			}	// case Message.REQ_ACCESS
			case Messages.CTS_REGID:
			{
				switch(result)
				{
				case Messages.APPROVED:	// 접속 허가
					MainActivity activity = (MainActivity)order.getContext();
					activity.Callback_RespRegidApproved();
					break;
				}	// case Messages.APPROVED
				break;
			}	// case Messages.CTS_REGID
			case Messages.REQ_SIGNUP:
			{
				switch(result)
				{
				case Messages.APPROVED:	// 가입 승인
					Toast.makeText(order.getContext(), order.getContext().getString(R.string.info_approved_signup), Toast.LENGTH_SHORT).show();
					((SignupActivity)order.getContext()).finish();
					break;
					
				case Messages.REFUSAL:	// ID중복있음
					Toast.makeText(order.getContext(), order.getContext().getString(R.string.error_already_email), Toast.LENGTH_SHORT).show();
					break;
				}
				break;
			}	// case Messages.REQ_SIGNUP
			case Messages.REQ_LOGOUT:
			{
				switch(result)
				{
				case Messages.APPROVED:	// 로그아웃 승인...밖에 나올게 없다.
					Toast.makeText(order.getContext(), order.getContext().getString(R.string.info_approved_logout), Toast.LENGTH_SHORT).show();
					Token.setUserToken(order.getContext(), 0);
					break;
				}
				break;
			}	// case Messages.REQ_LOGOUT
			case Messages.REQ_ADDFRIEND:
			{
				// REQ_ADDFRIEND 메시지는 다이얼로그에서 송신한다.
				switch(result)
				{
				case Messages.NOTFOUND:
				{
					Toast.makeText(ApplicationContextProvider.getContext(), 
							ApplicationContextProvider.getContext().getString(R.string.error_notfound),
							Toast.LENGTH_SHORT).show();
					
					break;
				}	// case Messages.NOTFOUND
				case Messages.ALREADY:
				{
					Toast.makeText(ApplicationContextProvider.getContext(), 
							ApplicationContextProvider.getContext().getString(R.string.error_already_friend),
							Toast.LENGTH_SHORT).show();
					
					break;
				}	// case Messages.ALREADY:
				case Messages.APPROVED:
				{
					Toast.makeText(ApplicationContextProvider.getContext(), 
							ApplicationContextProvider.getContext().getString(R.string.info_approved_addfriend),
							Toast.LENGTH_SHORT).show();
					
					MapActivity map = (MapActivity)order.getContext();
					if(map.mAddFriendDlg != null)
						map.mAddFriendDlg.dismiss();
					
					break;
				}
				}
				break;
			}	// case Messages.REQ_ADDFRIEND
			case Messages.REQ_GETFRIENDREQLIST:
			{
				switch(result)
				{
				case Messages.NOTFOUND:
				{
					Toast.makeText(order.getContext(), order.getContext().getString(R.string.info_empty_friendreqlist),
							Toast.LENGTH_SHORT).show();
					break;
				}	// case Messages.NOTFOUND
				}
				break;
			}	// case Messages.REQ_GETFRIENDREQLIST
			case Messages.REQ_RESPFRIENDREQ:
			{
				switch(result)
				{
				case Messages.REFUSAL:	// 거절당함
				{
					Toast.makeText(order.getContext(), order.getContext().getString(R.string.error_unknown), Toast.LENGTH_SHORT).show();
					break;
				}	// case Messages.REFUSAL
				case Messages.APPROVED:	// 승인됨
				{
					MapActivity activity = (MapActivity)order.getContext();
					int[] data = (int[])order.getObj();	// data[0]=승낙/거절 , data[1]=순번
					// 승낙했던 거절했던 일단 삭제
					if(activity.mFriendReqDlg != null)
						activity.mFriendReqDlg.removdItem(data[1]);
					String msg;
					if(data[0] == Messages.APPROVED)	// 친구요청에 대한 대답이 승낙이였다면
						msg = order.getContext().getString(R.string.info_approved_friendreq);
					else
						msg = order.getContext().getString(R.string.info_refusal_friendreq);
					
					Toast.makeText(order.getContext(), msg, Toast.LENGTH_SHORT).show();						
					break;
				}	// case Messages.APPROVED
				}	// switch(result)
				break;
			}	// case Messages.REQ_RESPFRIENDREQ
			}
			break;
		}	// case Messages.RESP_RESULT
		
		case Messages.RESP_DEVICETOKEN:
		{
			/*	RESP_DEVICETOKEN Struct Members
			 *  [TYPE]			[NAME]		[SIZE]		[COMMAND]
			 *  TOKEN			device		8			디바이스 토큰 
			 */
			
			long token = bb.getLong(pos);
			
			Token.setDeviceToken(order.getContext(), token);
			byte[] REGID_BUF = Messages.BUILD_CTS_REGID(GCMServiceInfo.regId);
			new TCP().execute(new SessionOrder(order.getContext(), MessageType.SEND, REGID_BUF));
			
			break;	// case Messages.RESP_DEVICETOKEN
		}
		
		case Messages.RESP_USERTOKEN:
		{
			/*	RESP_USERTOKEN Struct Members
			 *  [TYPE]			[NAME]		[SIZE]		[COMMAND]
			 *  TOKEN			user		8			디바이스 토큰 
			 */
			LoginActivity login_activity = (LoginActivity)order.getContext();
			
			long token = bb.getLong(pos);
			
			Token.setUserToken(login_activity, token);
			login_activity.callback_RespLogin_Accept();
			break;
		}	// case Messages.RESP_USERTOKEN:
		
		case Messages.RESP_LOCATIONDATA:
		{	
			/*	MSG_RESP_LOCATIONDATA Struct Members
			 *  [TYPE]			[NAME]		[SIZE]		[COMMAND]
			 *	int				cnt			4			locationdata 갯수
			 * 	LOCATIONDATA	pList		55 * cnt	위치정보
			 */
			MapActivity map_activity = (MapActivity)order.getContext();
			ArrayList<LocationData> list = new ArrayList<LocationData>();
			int cnt = bb.getInt(pos);
			pos += 4;
			
			for(int k = 0; k<cnt; k++)
			{
				long usertoken = bb.getLong(pos);
				pos += 8;
				
				double latitude = bb.getDouble(pos);
				pos += 8;

				double longitude = bb.getDouble(pos);
				pos += 8;
				
				int speed = bb.getInt(pos);
				pos += 4;

				int bearing = bb.getInt(pos);
				pos += 4;

				int accuracy = bb.getInt(pos);
				pos += 4;
				
				String date = new String(buf, pos, 19);
				pos += 19;
				
				list.add(new LocationData(usertoken, latitude, longitude, speed, bearing, accuracy, date));
			}
			map_activity.addMarker(list);
			map_activity.DrawOverlay(true);
		}
		break;	// case Messages.RESP_LOCATIONDATA
		
		case Messages.RESP_ALARMZONE:
		{
			/*	MSG_RESP_ALARMZONE Struct Members
			 *  [TYPE]			[NAME]				[SIZE]		[COMMAND]
			 *  int				cnt					4			알람존 갯수
			 * 	ALARMZONE		pList				78 * cnt	알람존 DATA 
			 */
			MapActivity map_activity = (MapActivity)order.getContext();
			ArrayList<ALARMZONE> list = new ArrayList<ALARMZONE>();

			int cnt = bb.getInt(pos);
			pos += 4;
			for(int i = 0; i<cnt; i++)
			{
				long idx = bb.getLong(pos);
				pos += 8;
				
				double latitude = bb.getDouble(pos);
				pos += 8;

				double longitude = bb.getDouble(pos);
				pos += 8;
				
				int distance = bb.getInt(pos);
				pos += 4;
				
				String strNick = new String(buf, pos, 50);
				pos += 50;
				
				list.add(new ALARMZONE(idx, latitude, longitude, distance, strNick));			
			}
			map_activity.addAlarmzone(list);
			map_activity.DrawOverlay(false);
		}
		break;	// case Messages.RESP_ALARMZONE
		case Messages.RESP_ADDALARMZONE:
		{
			/*	MSG_RESP_ADDALARMZONE Struct Members
			 *  [TYPE]			[NAME]				[SIZE]		[COMMAND]
			 *  int				cnt					8			알람존 인덱스
			 */
			
			long idx = bb.getLong(pos);
			ALARMZONE zone = (ALARMZONE)order.getObj();
			if(zone != null)
				zone.setIndex(idx);
			
			MapActivity map_activity = (MapActivity)order.getContext();
			map_activity.addAlarmzone(zone);
			map_activity.DrawOverlay(false);
			break;
		}	// case Messages.RESP_ADDALARMZONE
		case Messages.RESP_GETMYDATA:
		{
			/*	MSG_RESP_GETMYDATA Struct Members
			 *  [TYPE]			[NAME]				[SIZE]		[COMMAND]
			 *	TOKEN 			nUserToken			8			유저 토큰
			 *	int 			nId_size			4			아이디 길이
			 *	int 			nName_size			4			이름 길이
			 *	char[] 			buf					<=82		아이디, 이름 합성 버퍼
			 */
			
			UserInfo.UserToken = bb.getLong(pos);
			pos += 8;
			int id_size = bb.getInt(pos);
			pos += 4;
			int name_size = bb.getInt(pos);
			pos += 4;
			
			UserInfo.Email = new String(buf, pos, id_size);
			pos += id_size;
			UserInfo.Name = new String(buf, pos, name_size);
			pos += name_size;
			
			if(order.getContext() instanceof MainActivity)
			{
				MainActivity main = (MainActivity)order.getContext();
				main.Callback_RespMyData();
			}
			else if(order.getContext() instanceof LoginActivity)
			{
				LoginActivity login = (LoginActivity)order.getContext();
				login.Callback_RespMyData();
			}
			else
			{
				Toast.makeText(order.getContext(), order.getContext().getString(R.string.error_unknown), Toast.LENGTH_SHORT).show();
			}
			break;
		}	// case Messages.RESP_GETMYDATA
		case Messages.RESP_GETFRIENDREQLIST:
		{
			/*	MSG_RESP_GETFRIENDREQLIST Struct Members
			 *  [TYPE]			[NAME]			[SIZE]			[COMMAND]
			 *  int				nCnt			8				데이터 갯수
			 *  FRIENDREQLIST	pData			<= (109*nCnt)	데이터
			 */
			
			/*	FRIENDREQLIST Struct Members
			 *  [TYPE]			[NAME]				[SIZE]		[COMMAND]
			 * 	int				email_len			4			이메일 길이
			 * 	int				name_len			4			이름 길이
			 * 	DATETIME_STRING	datetime			19			DATETIME 문자열
			 * 	char[]			buf					<= 82		이메일, 이름 합성버퍼
			 */
			
			int nCnt = bb.getInt(pos);
			pos += 4;
			
			ArrayList<FriendInfo> list = new ArrayList<FriendInfo>(nCnt);
			for(int i = 0; i<nCnt; i++)
			{
				FriendInfo info = new FriendInfo();
				
				int email_len = bb.getInt(pos);
				pos += 4;
				int name_len = bb.getInt(pos);
				pos += 4;
				info.datetime = new String(buf, pos, 19);
				pos += 19;
				info.email = new String(buf, pos, email_len);
				pos += email_len;
				info.name = new String(buf, pos, name_len);
				pos += name_len;
				
				list.add(info);
			}
			
			MapActivity map = (MapActivity)order.getContext();
			map.callback_resp_friendreqlist(list);
			
			break;
		}	// case Messages.RESP_GETFRIENDREQLIST
		case Messages.RESP_GETFRIENDLIST:
		{
			/*	MSG_RESP_GETFRIENDLIST Struct Members
			 *  [TYPE]			[NAME]			[SIZE]			[COMMAND]
			 *  int				nCnt			8				데이터 갯수
			 *  FRIENDLIST		pData			<= (163*nCnt)	데이터
			 */
			
			/*	FRIENDLIST Struct Members
			 *  [TYPE]			[NAME]			[SIZE]		[COMMAND]
			 * 	int				email_len		4			이메일 길이
			 * 	int				name_len		4			이름 길이
			 * 	int				relation_len	4			관계 길이
			 * 	DATETIME_STRING	datetime		19			DATETIME 문자열
			 * 	char[]			buf				<= 132		이메일, 이름 합성버퍼
			 */
			
			int nCnt = bb.getInt(pos);
			pos += 4;
			
			ArrayList<FriendInfo> list = new ArrayList<FriendInfo>(nCnt);
			for(int i = 0; i<nCnt; i++)
			{
				FriendInfo info = new FriendInfo();
				
				int email_len = bb.getInt(pos);
				pos += 4;
				int name_len = bb.getInt(pos);
				pos += 4;
				int relation_len = bb.getInt(pos);
				pos += 4;
				info.datetime = new String(buf, pos, 19);
				pos += 19;
				info.email = new String(buf, pos, email_len);
				pos += email_len;
				info.name = new String(buf, pos, name_len);
				pos += name_len;
				info.relationship = new String(buf, pos, relation_len);
				pos += relation_len;
				
				list.add(info);
			}
			
			MapActivity map = (MapActivity)order.getContext();
			map.callback_resp_friendlist(list);
			
			break;
		}	// case Messages.RESP_GETFRIENDLIST
		}//switch
		
		return true;
	}

	protected Result Send(byte[] buf)
	{
		if(isConnect() == false)
		{
			error_msg = "Not Connected";
			Log.d("TCP", "Send, Not Connected");
			return Result.Fail;
		}
		
		try{
			out.write(buf);
			out.flush();	// 송신
			Log.i("TCP", "Send buf");
			
			order.setType(MessageType.RECV);
			new TCP().execute(order);
			//if(Recv() != Result.Done)
			//	return Result.Fail;
			
		} catch (IOException e)
		{
			e.printStackTrace();
			error_msg = e.toString();
			Log.e("TCP", "Send, IOException");
			return Result.Fail;
		}
		
		return Result.Done;
	}
	
	protected Result Recv()
	{
		if(isConnect() == false)
		{
			error_msg = "Not Connected";
			Log.d("TCP", "Send, Not Connected");
			return Result.Fail;
		}
		
		Log.i("TCP", "Recv...");
		
		byte[] buf = new byte[buf_size];
		byte[] total_packet = null;
		try{
			int msg_size = 0;
			int size = 0;
			int total = 0;
			
			while((size = in.read(buf)) != -1)
			{
				int total_packet_pos = 0;
				total += size;
				byte[] before_buf = total_packet;
				total_packet = new byte[total];
				if(before_buf != null)
				{
					total_packet_pos = before_buf.length;
					System.arraycopy(before_buf, 0, total_packet, 0, total_packet_pos);	// 버퍼 교체
				}
				System.arraycopy(buf, 0, total_packet, total_packet_pos, size);	// 버퍼에 데이터 삽입
				
				// 수신한 데이터의 크기와 패킷의 실제 크기를 비교
				if(msg_size == 0 && total >= 0)
				{
					// 빈 상태에서 처음 수신한경우 +4위치에 패킷의 크기가 존재함
					// 헤더를 임시로 복사하여 헤더의 크기를 읽어 패킷의 총 크기를 파악
					byte[] header = new byte[8];
					System.arraycopy(total_packet, 0, header, 0, 8);
					ByteBuffer bb = ByteBuffer.allocate(8);
					bb.order(ByteOrder.LITTLE_ENDIAN);
					bb.put(header);
					msg_size = bb.getInt(4);
				}
				if(msg_size <= total)	// 목표치 달성
					break;
			}
			
			RecvSize = total;
			RecvBuf = total_packet;
		} catch (IOException e){
			e.printStackTrace();
			Log.e("TCP", "Recv, IOException");
			return Result.Fail;
		}
		
		return Result.Done;
	}
	
	@Override
	protected TCP.Result doInBackground(SessionOrder... params) {
		// TODO Auto-generated method stub
		
		Result res = Result.Done;
		order = params[0];
		
		switch(order.getType())
		{
		case CONNECT:
			res = Connect();
			break;
			
		case DISCONNECT:
			Disconnect();
			break;
			
		case SEND:
			res = Send(order.getBuf());
			break;
			
		case RECV:
			res = Recv();
			break;
			
		default:
			break;
		}
		
		return res;
	}
	
	@Override
	protected void onPostExecute(Result res)
	{
		switch(order.getType())
		{
		case RECV:
			if(RecvSize > 0)
				ProcessMsg(RecvBuf);
			break;
			
		case CONNECT:
			MainActivity activity = (MainActivity)order.getContext();
			activity.callback_connected();
			break;
		}
	}
}
