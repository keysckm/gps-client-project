package com.htna.imhere;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.R.color;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentManager.OnBackStackChangedListener;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.GoogleMap.OnMyLocationButtonClickListener;
import com.google.android.gms.maps.GoogleMap.OnMyLocationChangeListener;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.SupportMapFragment;
import com.htna.imhere.SessionOrder.MessageType;

public class MapActivity extends FragmentActivity implements OnMarkerClickListener {

	public class MapActivityHandler extends Handler{

		static final int MSG_UPDATE_LOCATION = 10;
		
		@Override
		public void handleMessage(Message msg){
			switch(msg.arg1)
			{
			case MSG_UPDATE_LOCATION:
				break;
			}
		}
	}
	
	public static MapActivity Activity = null;			// Activity
	public MapActivityHandler mHandler = null;			// 핸들러
	
	private ArrayList<ALARMZONE>	mAlarmzones = null;	// Alarmzone List
	private ArrayList<LocationData>	mMarkers = null;	// GPSData List
	private GoogleMap 			mGoogleMap = null;		// 구글맵 객체 초기화
	private Geocoder			mGc = null;				// 좌표 <->주소 전환을 위한 지오코더
		
	private LatLng				mLongClickPosition = null;	// 롱클릭 위치 경위도값
	private Circle				mLastShowCircle = null;	// 마지막으로 표시된 circle 객체
	
	private MyLocManager		mGpsListener = null;
	private Location			mGpsMyLocation = null;	// 현재 위치가 업데이트되면 이 객체를 현재 위치로
	
	boolean misTrackingMyLocation = true;				// 내 위치가 변경되면 카메라가 따라갈지 설정
	
	// 서버에서 온 요청에 대해 다이얼로그를 조작할 방법이 딱히 생각안나서...
	AddFriendDialog				mAddFriendDlg = null;	// 친구 추가 요청 다이얼로그
	FriendListDialog			mFriendReqDlg = null;	// 친구 요청자 리스트 다이얼로그
	FriendListDialog			mFriendListDlg	= null;	// 친구 목록 리스트
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_map);

	    Activity = this;
	    mHandler = new MapActivityHandler();
	    
	    mGpsListener = new MyLocManager();
	    ReceiveLocation(true);
	    
	    init_member_field();	// 필드 초기화
	    
	    // 컨텍스트 메뉴 등록
	    RelativeLayout layout = (RelativeLayout)findViewById(R.id.MapRelativeLayout); 
	    registerForContextMenu(layout);
	    
	    // google map object를 초기화한다.
        init_googlemap_object();
        // 카메라의 위치를 초기화한다.
        init_camera_pos();
        
        // 알람존 데이터 요청
        Request_Alarmzone();
	}
	
	// GPS 수신 설정
	public void ReceiveLocation(boolean enabled)
	{
		 // LocationManager 요청
	    LocationManager manager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
	    if(manager == null)
	    {
	    	Log.e("GPS", "LocationManager를 사용할 수 없음");
	    	Toast.makeText(this, getString(R.string.error_locationmanager_notready), Toast.LENGTH_LONG).show();
	    	return;
	    }
	    
		if(enabled)
		{
		    boolean isGps = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);	// GPS provider를 사용할 수 있는지 확인
		    boolean isNetwork = manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);	// 네트워크 provide를 사용할 수 있는지 확인
		    if(isGps)
		    {
		    	Log.i("GPS", "Start Recv Location from GPS Provider");
		    	manager.requestLocationUpdates(
		    		LocationManager.GPS_PROVIDER, 10000, 10, mGpsListener);
		    }
		    else if(isNetwork)
		    {
		    	Log.i("GPS", "Start Recv Location from Network Provider");
		    	manager.requestLocationUpdates(
		    		LocationManager.NETWORK_PROVIDER, 10000, 10, mGpsListener);
		    }
		    else
		    {
		    	Log.e("GPS", "Any Provider is Not available");
		    	Toast.makeText(this, getString(R.string.error_location_notready), Toast.LENGTH_LONG).show();
		    }
		}
		else
		{
			manager.removeUpdates(mGpsListener);
		}
	}
	
	@Override
	public void onResume(){
		super.onResume();
		if(MapActivity.Activity != null)
		{
			mGoogleMap.setMyLocationEnabled(true);
			mGoogleMap.setOnMyLocationChangeListener(new OnMyLocationChangeListener(){

				@Override
				public void onMyLocationChange(Location arg0) {
					// TODO Auto-generated method stub
					
					mGpsMyLocation = arg0;
					
					UserInfo.Latitude = arg0.getLatitude();
					UserInfo.Longitude = arg0.getLongitude();
					UserInfo.Speed = (int)arg0.getSpeed();
					UserInfo.Bearing = (int)arg0.getBearing();
					
					if(misTrackingMyLocation)
						SetCameraPos(new LatLng(arg0.getLatitude(), arg0.getLongitude()), true);
				}
				
			});
			mGoogleMap.setOnMyLocationButtonClickListener(new OnMyLocationButtonClickListener(){

				@Override
				public boolean onMyLocationButtonClick() {
					if(misTrackingMyLocation == false)
					{
						misTrackingMyLocation = true;
						Toast.makeText(Activity, getString(R.string.info_tracking_camera), Toast.LENGTH_SHORT).show();
					}
					return false;
				}
				
			});
		}
	}
	
	@Override
	public void onPause(){
		super.onPause();
		if(MapActivity.Activity != null)
			mGoogleMap.setMyLocationEnabled(false);
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuinfo)
	{
		menu.add(getString(R.string.label_menu_addalarmzone));
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		menu.add(getString(R.string.label_menu_mydata));
		menu.add(getString(R.string.label_menu_tracking));
		menu.add(getString(R.string.label_menu_addfriend));
		menu.add(getString(R.string.label_menu_friendlist));
		menu.add(getString(R.string.label_menu_friendreqlist));
		menu.add(getString(R.string.label_menu_logout));
		return true;
	}
	
	public void callback_resp_friendlist(ArrayList<FriendInfo> list)
	{
		mFriendListDlg = new FriendListDialog(this, new FriendListWidget.OnDataSelectionListener() {
			@Override
			public void onDataSelected(AdapterView<?> parent, View view,
					int position, long id) {
			}
		});
		// 아이템 등록
		for(FriendInfo info : list)
			mFriendListDlg.addItem(info);
		
		mFriendListDlg.setTitle(R.string.dlg_approved_friendreqlist_title);
		mFriendListDlg.show();
	}
	
	public void callback_resp_friendreqlist(ArrayList<FriendInfo> list)
	{
		mFriendReqDlg = new FriendListDialog(this, new FriendListWidget.OnDataSelectionListener() {
			
			@Override
			public void onDataSelected(AdapterView<?> parent, View view, int position,
					long id) {
				
				FriendListView friendview = (FriendListView)view;
				
				final int pos = position;
				final String email = friendview.getEmail();
				final String name = friendview.getName();
				final String datetime = friendview.getDatetime();
				
				AlertDialog.Builder builder = new AlertDialog.Builder(Activity);
				builder.setTitle(R.string.dlg_approved_friendreq_title);
				builder.setMessage(name + getString(R.string.dlg_approved_friendreq_content));
				builder.setPositiveButton(R.string.button_approved, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// 승낙
						byte[] buf = Messages.BUILD_REQ_RESPFRIENDREQ(email, Messages.APPROVED);
						SessionOrder order = new SessionOrder(Activity, MessageType.SEND, buf);
						int[] data = new int[2];
						data[0] = Messages.APPROVED;	// 승낙함
						data[1] = pos;					// pos번째 데이터
						order.setObj(data);				// 데이터 삽입
						new TCP().execute(order);
					}
				});
				builder.setNegativeButton(R.string.button_refusal, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// 거절
						byte[] buf = Messages.BUILD_REQ_RESPFRIENDREQ(email, Messages.REFUSAL);
						SessionOrder order = new SessionOrder(Activity, MessageType.SEND, buf);
						int[] data = new int[2];
						data[0] = Messages.APPROVED;	// 거절함
						data[1] = pos;					// pos번째 데이터
						order.setObj(data);				// 데이터 삽입
						new TCP().execute(order);
					}
				});
				builder.setNeutralButton(R.string.button_cancel, new DialogInterface.OnClickListener(){
					@Override
					public void onClick(DialogInterface dialog, int which) {}
				});
				
				builder.create().show();
			}
		}); 
		
		// 아이템 등록
		for(FriendInfo info : list)
			mFriendReqDlg.addItem(info);
		
		mFriendReqDlg.setTitle(R.string.dlg_approved_friendreqlist_title);
		mFriendReqDlg.show();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		CharSequence select = item.getTitle();
		if(select.equals(getString(R.string.label_menu_tracking)))
		{
			final GetdateDialog d = new GetdateDialog(this);
			d.setTitle(getString(R.string.dlg_tracking_title));
			d.setOnDismissListener(new OnDismissListener(){
			
				@Override
				public void onDismiss(DialogInterface dialog) {
					// TODO Auto-generated method stub
					if(d.misOk == false)
						return;
					String strDateFormat = "yyyy-MM-dd HH:mm:ss";
					SimpleDateFormat df = new SimpleDateFormat(strDateFormat);
					String strStartDate = df.format(d.getStartCal().getTime());
					String strEndDate = df.format(d.getEndCal().getTime());
					
					byte[] buf = Messages.BUILD_REQ_LOCATIONDATA(strStartDate, strEndDate);
					
					new TCP().execute(new SessionOrder(MapActivity.this, MessageType.SEND,
							buf));
				}
				
			});
			
			d.show();
		}
		else if(select.equals(getString(R.string.label_menu_mydata)))
		{
			final MyDataDialog d = new MyDataDialog(this);
			d.setTitle(getString(R.string.dlg_mydata_title));
			d.show();
		}
		else if(select.equals(getString(R.string.label_menu_addfriend)))
		{
			mAddFriendDlg = new AddFriendDialog(this);
			mAddFriendDlg.setTitle(R.string.dlg_addfriend_title);
			mAddFriendDlg.show();
		}
		else if(select.equals(getString(R.string.label_menu_logout)))
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle(R.string.dlg_logout_title);
			builder.setMessage(R.string.dlg_logout_content);
			builder.setPositiveButton(R.string.button_yes, new DialogInterface.OnClickListener(){
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					// 위치추적 중지
					mGoogleMap.setMyLocationEnabled(false);
					ReceiveLocation(false);
					
					byte[] buf = Messages.BUILD_REQ_LOGOUT();
					new TCP().execute(new SessionOrder(Activity, MessageType.SEND, buf));
					startActivity(new Intent(ApplicationContextProvider.getContext(), LoginActivity.class));
					UserInfo.clear();
					
					finish();
				}
			});
			builder.setNegativeButton(R.string.button_no, new DialogInterface.OnClickListener(){
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
				}
			});
			
			AlertDialog dialog = builder.create();
			dialog.show();
		}
		else if(select.equals(getString(R.string.label_menu_friendreqlist)))
		{
			byte[] buf = Messages.BUILD_REQ_GETFRIENDREQLIST();
			new TCP().execute(new SessionOrder(Activity, MessageType.SEND, buf));
		}
		else if(select.equals(getString(R.string.label_menu_friendlist)))
		{
			byte[] buf = Messages.BUILD_REQ_GETFRIENDLIST();
			new TCP().execute(new SessionOrder(Activity, MessageType.SEND, buf));
		}
		
		return false;
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item)
	{
		if(item.getTitle().equals(getString(R.string.label_menu_addalarmzone)))
		{
			final AddAlarmzoneDialog dlg = new AddAlarmzoneDialog(MapActivity.this);
			dlg.setLatitude(mLongClickPosition.latitude);
			dlg.setLongitude(mLongClickPosition.longitude);
			dlg.setTitle(getString(R.string.dlg_addalarmzone_title));
			dlg.setAddress(getAddressFromCoordinate(mLongClickPosition));
			
			dlg.setOnDismissListener(new OnDismissListener(){

				@Override
				public void onDismiss(DialogInterface dialog) {
					// TODO Auto-generated method stub
					if(dlg.misOk == false)
						return;
					
					int nDist = dlg.getDistance();
					String strNick = dlg.getNickname();
					
					byte[] buf = Messages.BUILD_REQ_ADDALARMZONE(mLongClickPosition.latitude, mLongClickPosition.longitude,
							nDist, strNick);
					
					SessionOrder order = new SessionOrder(MapActivity.this, MessageType.SEND, buf);
					
					ALARMZONE s = new ALARMZONE(mLongClickPosition.latitude, mLongClickPosition.longitude,
							nDist, strNick);
					
					order.setObj(s);
					new TCP().execute(order);
				}
				
			});
			
			dlg.show();
		}
		return false;
	}
	
	@Override
	public void onContextMenuClosed(Menu menu)
	{
		
	}
	
	protected void init_member_field()
	{
		mAlarmzones = new ArrayList<ALARMZONE>();
	    mMarkers = new ArrayList<LocationData>();
	    mGc = new Geocoder(this, Locale.KOREAN);
	}
    
    protected void init_googlemap_object()
    {
        FragmentManager fm = getSupportFragmentManager();
        Fragment f = fm.findFragmentById(R.id.map);
        mGoogleMap = ((SupportMapFragment)f).getMap();
        
        // 마커 클릭 리스너 설정
        mGoogleMap.setOnMarkerClickListener(this);
        mGoogleMap.setOnMapClickListener(new OnMapClickListener(){

			@Override
			public void onMapClick(LatLng point) {
				// TODO Auto-generated method stub
				if(mLastShowCircle != null)
				{
					mLastShowCircle.remove();
					mLastShowCircle = null;
				}
			}
        	
        });
        
        mGoogleMap.setOnMapLongClickListener(new OnMapLongClickListener(){

			@Override
			public void onMapLongClick(LatLng point) {
				// TODO Auto-generated method stub
				mLongClickPosition = point;
				openContextMenu(findViewById(R.id.map));
			}
        	
        });
        
        mGoogleMap.setOnCameraChangeListener(new OnCameraChangeListener(){

			@Override
			public void onCameraChange(CameraPosition arg0) {
				// TODO Auto-generated method stub
				if(mGpsMyLocation == null)
					return;
				
				LatLng now = new LatLng(mGpsMyLocation.getLatitude(), mGpsMyLocation.getLongitude());
				if(!now.equals(arg0.target) && misTrackingMyLocation == true)
				{	// 카메라가 향하는곳이 현재위치가 아니라면 다른곳을 보고자 함
					Log.d("map", "Disable tracking camera MyLocation");
					misTrackingMyLocation = false;
				}
			}
        	
        });
    }
    
    protected void init_camera_pos()
    {
    	LatLng loc = new LatLng(37.5559, 126.9723);	// 기준좌표는 서울역
    	SetCameraPos(loc, false);
    }
    
    protected void Request_Alarmzone()
    {
    	mAlarmzones.clear();
    	
    	byte[] buf = Messages.BUILD_REQ_GETALARMZONE();
    	new TCP().execute(new SessionOrder(this, MessageType.SEND, buf));
    }
    
    public String getAddressFromCoordinate(LatLng loc)
    {
    	String addr = "";
    	List<Address> addressList = null;
    	try
    	{
    		addressList = mGc.getFromLocation(loc.latitude, loc.longitude, 1);
    		if(addressList != null)
    		{
    			addr = addressList.get(0).getAddressLine(0);
    		}
    	}
    	catch(IOException e)
    	{
    		Log.e("Geocoder", e.getMessage());
    		return "";
    	}
    	
    	return addr;
    }
    
    public void addAlarmzone(List<ALARMZONE> list)
    {
    	for(ALARMZONE s : list)
    		mAlarmzones.add(s);
    }
    
    public void addAlarmzone(ALARMZONE s)
    {
    	mAlarmzones.add(s);
    }
    
    public void addMarker(List<LocationData> list)
    {
    	for(LocationData g : list)
    		mMarkers.add(g);	// 마커 데이터 추가
    }
    
    public void addMarker(LocationData g)
    {
    	mMarkers.add(g);
    }
    
    public void DrawOverlay(boolean iscameramove)
    {
    	mGoogleMap.clear();
    	DrawMarkers(iscameramove);
    	DrawAlarmzones();
    }
    
    public void DrawMarkers(boolean iscameramove)
    {
    	ArrayList<PolylineOptions> polyline_list = new ArrayList<PolylineOptions>();
    	PolylineOptions polyline = null;	// 폴리라인 객체
    	LatLng loc = null;
    	long before_token = 0;
    	long now_token = 0;
    	for(LocationData g : mMarkers)
    	{
    		before_token = now_token;
    		now_token = g.getUserToken();
    		if(now_token != before_token)
    		{
    			if(polyline != null)
    				polyline_list.add(polyline);
    			
    			polyline = new PolylineOptions();
    		}
    		loc = new LatLng(g.getLatitude(), g.getLongitude());	// 데이터로부터 좌표 획득
    		// 마커 추가(나중에 아이콘 바꿔야함)
    		MarkerOptions marker = new MarkerOptions();
    		marker.position(loc);
    		marker.title(g.getDate());
    		marker.snippet(getAddressFromCoordinate(loc));
    		marker.draggable(false);
    		g.setMarker(mGoogleMap.addMarker(marker));
    		
    		polyline.add(loc);	// 폴리라인 추가
    	}
    	if(polyline != null)
    		polyline_list.add(polyline);
    	
    	if(loc != null)	// 좌표데이터가 없다면 수신된 좌표가 없다는거임. 문제가 있는거지.
    	{
    		if(iscameramove == true)
    			SetCameraPos(loc, true);			// 맵 카메라 위치 이동
    		
    		for(PolylineOptions p : polyline_list)
    		{
    			mGoogleMap.addPolyline(p);	// 오버레이 등록
    		}
    	}
    }
    public void DrawAlarmzones()
    {
    	for(ALARMZONE s : mAlarmzones)
    	{
    		MarkerOptions safezone = new MarkerOptions();
    		safezone.title(s.getNickname());
    		safezone.snippet("반경 " + Integer.toString(s.getDistance()) + "m");
    		safezone.draggable(false);
    		safezone.position(new LatLng(s.getLatitude(), s.getLongitude()));
        	s.setMarker(mGoogleMap.addMarker(safezone));

        	CircleOptions circles = new CircleOptions();
        	circles.center(new LatLng(s.getLatitude(), s.getLongitude()));
        	circles.radius(s.getDistance());
        	circles.strokeWidth(3);
        	circles.strokeColor(Color.argb(255, 0, 255, 0));
        	// circles.fillColor(Color.argb(100, 0, 255, 0));
        	
        	mGoogleMap.addCircle(circles);
    	}
    }
    
    public void SetCameraPos(LatLng loc, boolean animated)
    {
    	if(animated == true)
        	mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, 15));
    	else
    		mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(loc, 15));
    }

	@Override
	public boolean onMarkerClick(Marker arg0) {
		// TODO Auto-generated method stub
		LocationData targ = null;
		
		for(LocationData g : mMarkers)
		{
			if(arg0.equals(g.getMarker()))
				targ = g;
		}
		if(targ == null)
			return false;
		
		if(mLastShowCircle != null)
			mLastShowCircle.remove();
		
		CircleOptions circle = new CircleOptions();
		circle.center(new LatLng(targ.getLatitude(), targ.getLongitude()));
		circle.radius(targ.getAccuracy());
		circle.strokeWidth(3);
		circle.strokeColor(Color.BLUE);
		// circle.fillColor(Color.argb(100, 0, 0, 255));
		mLastShowCircle = mGoogleMap.addCircle(circle);
		
		return false;
	}
}

enum MarkerType{
	TYPE_GPS,
	TYPE_SAFEZONE,
}

class MarkerObject
{
	private Marker		mMarker = null;
	private MarkerType	mType;

	public Marker getMarker() {
		return mMarker;
	}

	public void setMarker(Marker mMarker) {
		this.mMarker = mMarker;
	}

	public MarkerType getType() {
		return mType;
	}

	public void setType(MarkerType mType) {
		this.mType = mType;
	}
	
}

class ALARMZONE extends MarkerObject
{
	private long	mIdx;
	private double 	mLatitude;
	private double 	mLongitude;
	private int 	mDistance;
	private String	mNickname;
	
	public ALARMZONE(long _nIdx, double _dLat, double _dLon, int _nDis, String _strNick)
	{
		super.setType(MarkerType.TYPE_SAFEZONE);
		mIdx = _nIdx;
		mLatitude = _dLat;
		mLongitude = _dLon;
		mDistance = _nDis;
		mNickname = _strNick;
	}
	
	public ALARMZONE(double _dLat, double _dLon, int _nDis, String _strNick)
	{
		super.setType(MarkerType.TYPE_SAFEZONE);
		mLatitude = _dLat;
		mLongitude = _dLon;
		mDistance = _nDis;
		mNickname = _strNick;
	}
	
	public double getLatitude() {
		return mLatitude;
	}
	public void setLatitude(double _Latitude) {
		this.mLatitude = _Latitude;
	}
	public double getLongitude() {
		return mLongitude;
	}
	public void setLongitude(double _Longitude) {
		this.mLongitude = _Longitude;
	}
	public int getDistance() {
		return mDistance;
	}
	public void setDistance(int _Distance) {
		this.mDistance = _Distance;
	}
	public String getNickname() {
		return mNickname;
	}
	public void setNickname(String _Nickname) {
		this.mNickname = _Nickname;
	}
	public long getIndex()
	{
		return mIdx;
	}
	public void setIndex(long _Idx)
	{
		mIdx = _Idx;
	}
}

class LocationData extends MarkerObject
{
	private long		mUserToken;
	private double 		mLatitude;
	private double 		mLongitude;
	private int			mSpeed;
	private int			mBearing;
	private int			mAccuracy;
	private String		mDate;
	
	public LocationData(long _user, double _lat, double _long, int _speed, int _bearing, int _accuracy, String _date)
	{
		super.setType(MarkerType.TYPE_GPS);
		
		this.mUserToken = _user;
		this.mLatitude = _lat;
		this.mLongitude = _long;
		this.mSpeed = _speed;
		this.mBearing = _bearing;
		this.mAccuracy = _accuracy;
		this.mDate = _date;
	}
	public long getUserToken(){
		return mUserToken;
	}
	public void setUserToken(long _token)
	{
		mUserToken = _token;
	}
	public double getLongitude() {
		return mLongitude;
	}
	public void setLongitude(double longitude) {
		this.mLongitude = longitude;
	}
	public double getLatitude() {
		return mLatitude;
	}
	public void setLatitude(double latitude) {
		this.mLatitude = latitude;
	}
	public int getSpeed() {
		return mSpeed;
	}
	public void setSpeed(int speed) {
		this.mSpeed = speed;
	}
	public int getBearing() {
		return mBearing;
	}
	public void setBearing(int bearing) {
		this.mBearing = bearing;
	}
	public int getAccuracy() {
		return mAccuracy;
	}
	public void setAccuracy(int accuracy) {
		this.mAccuracy = accuracy;
	}
	public String getDate() {
		return mDate;
	}
	public void setDate(String date) {
		this.mDate = date;
	}
	
}
