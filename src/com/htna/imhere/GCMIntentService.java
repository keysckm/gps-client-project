package com.htna.imhere;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Message;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gcm.GCMBaseIntentService;

public class GCMIntentService extends GCMBaseIntentService {	
	
	public GCMIntentService()
	{
		super(GCMServiceInfo.PROJECT_ID);
		Log.i(GCMServiceInfo.TAG, "GSMService Instance");
	}
	
	@Override
	protected void onError(Context arg0, String arg1) {		
		Log.d(GCMServiceInfo.TAG, "Error: " + arg1);
	}
	@Override
	protected void onMessage(Context arg0, Intent arg1) {	
		String msg = arg1.getStringExtra("msg");
		String from = arg1.getStringExtra("from");
		String action = arg1.getStringExtra("action");
		Log.d(GCMServiceInfo.TAG, "data: "+ msg + ", " + ", " + from + ", " + action);
		
		NotificationManager n = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
		
		PendingIntent intent = PendingIntent.getActivity(getApplicationContext()
				, 0
				, new Intent(getApplicationContext(), MainActivity.class)
				, PendingIntent.FLAG_UPDATE_CURRENT);
		
		Notification noti_data = new NotificationCompat.Builder(getApplicationContext())
				.setContentTitle(getString(R.string.app_name))
				.setContentText(msg)
				.setSmallIcon(R.drawable.custom_launcher)
				.setTicker(msg)
				.setContentIntent(intent)
				.build();

		n.cancel(7777);
		n.notify(7777, noti_data);
	}
	@Override
	protected void onRegistered(Context arg0, String arg1) {		
		Log.d(GCMServiceInfo.TAG, "Registered Device: " + arg1);
		GCMServiceInfo.regId = arg1;
		
		Message msg = MainActivity.Activity.mHandler.obtainMessage();
		msg.arg1 = MainActivity.MainActivityHandler.MSG_ONREGIST;
		MainActivity.Activity.mHandler.sendMessage(msg);
	}
	@Override
	protected void onUnregistered(Context arg0, String arg1) {	
		Log.d(GCMServiceInfo.TAG, "Unregistered Device: " + arg1);
		GCMServiceInfo.regId = "";
	}	
	
}