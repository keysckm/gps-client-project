package com.htna.imhere;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;

public class FriendListWidget extends ListView{

	private Context mContext = null;
	private OnDataSelectionListener selectionListener = null;
	
	public FriendListWidget(Context context) {
		super(context);
		mContext = context;
		
		init();
	}
	
	void init()
	{
		setOnItemClickListener(new OnItemClickAdapter());
	}
	
	public void setOnDataSelectionListener(OnDataSelectionListener listener){
		selectionListener = listener;
	}
	
	public OnDataSelectionListener getOnDataSelectionListener()
	{
		return selectionListener;
	}
	
	// ��� ����
	public void setAdapter(BaseAdapter adapter){
		super.setAdapter(adapter);
	}
	
	// ��� ����
	public BaseAdapter getAdapter(){
		return (BaseAdapter)super.getAdapter();
	}

	class OnItemClickAdapter implements OnItemClickListener{

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			// TODO Auto-generated method stub
			if(parent == null)
				return;
			
			selectionListener.onDataSelected(parent, view, position, id);
		}
		
	}
	
	public interface OnDataSelectionListener {
	    public void onDataSelected(AdapterView<?> parent, View view, int position, long id);
	     
	}
}
